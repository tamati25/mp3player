package Data
{
	import flash.net.URLRequest;
	
	public class Song
	{
		private var path:URLRequest;
		private var title:String;
		private var artist:String;
		private var album:String;
		
		public function Song(url:URLRequest,
							 t:String="",
							 ar:String="Uknown Artist",
							 al:String="Unknown Album")
		{
			path = url;
			title = t;
			artist = ar;
			album = al;
		}
		
		public function toString():String{
			return title + ", " + artist + " ["+ album + "]";
		}
		
		public function getPath():URLRequest
		{
			return path;
		}
		
		public function getTitle():String
		{
			return title;
		}
		
		public function getArtist():String
		{
			return artist;
		}
		
		public function getAlbum():String
		{
			return album;
		}
		
		public function setPath(url:URLRequest):void
		{
			path = url;
		}
		
		public function setTitle(t:String):void
		{
			title = t;
		}
		
		public function setArtist(t:String):void
		{
			title = t;
		}
		
		public function setAlbum(t:String):void
		{
			title = t;
		}
	}
}