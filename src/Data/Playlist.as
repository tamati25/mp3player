package Data
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	import flash.xml.XMLDocument;
	import flash.xml.XMLNode;
	import mx.collections.ArrayCollection;
	
	import mx.controls.List;
	
	public class Playlist
	{
		private var songs:ArrayCollection;
		private var name:String;
		
		public function Playlist(n:String)
		{
			songs = new ArrayCollection();
			name = n;
		}
		
		
		/**
		 * Add a song to the playlist if not already in
		 * Return wether the song was added
		 **/
		public function addSong(s:Song):Boolean
		{
			for each (var s2:Song in songs)
			{
				if (s2.getTitle() == s.getTitle() &&
					s2.getArtist() == s.getArtist() &&
					s2.getAlbum() == s.getAlbum())
					return false;
			}
				songs.addItem(s);
				return true;
		}
		
		public function size():int
		{
			return songs.length;
		}
		
		public function getSong(s:String):Song{
			for (var i:int =0; i<songs.length; ++i)
				if (s == (songs[i].getTitle()))
					return songs[i];
			return null;
		}
		
		public function getIndex(s:Song):int{
			for (var i:int =0; i<songs.length; ++i)
			{
				if (songs[i].getTitle() == s.getTitle() &&
					songs[i].getArtist() == s.getArtist() &&
					songs[i].getAlbum() == s.getAlbum())
					return i;
			}
			return -1;
		}
		
		public function getSongAt(index:int):Song{
			if (songs.length > index)
					return songs[index];
			return null;
		}
		
		/**
		 * save the playlist in an xml file
		 **/
		public function save():Boolean
		{
			var xmldoc:XMLDocument = new XMLDocument(); 
			var file:File = new File(File.applicationDirectory.resolvePath( "myPlaylist.xml" ).nativePath );
			var fs:FileStream = new FileStream();
			fs.open(file, FileMode.WRITE);
			
			xmldoc.xmlDecl = '<?xml version="1.0" encoding="UTF-8"?>';
			
			var xmlroot:XMLNode = xmldoc.createElement("playlist");
			xmlroot.attributes.name = this.name;
			
			for each (var song:Song in this.songs)
			{
				var songNode:XMLNode = xmldoc.createElement("song");
				songNode.attributes.title = song.getTitle();
				songNode.attributes.artist = song.getArtist();
				songNode.attributes.album = song.getAlbum();
				songNode.attributes.path = song.getPath().url.toString();
				
				xmlroot.appendChild(songNode);
			}
			
			xmldoc.appendChild(xmlroot);
			
			fs.writeUTF(xmldoc.toString());
			fs.close();
			
			return true;
		}
		
		/**
		 * load the playlist from an xml file
		 **/
		public function load(filename:String):Boolean
		{
			var xmldoc:XMLDocument = new XMLDocument(); 
			xmldoc.ignoreWhite = true; 
			var fs:FileStream = new FileStream();
			var file:File = new File(File.applicationDirectory.resolvePath( "myPlaylist.xml" ).nativePath );
			fs.open(file, FileMode.READ);
			
			xmldoc.parseXML(fs.readUTF());
			
			this.name = "myPlaylist";
			
			for each(var songElt:XMLNode in xmldoc.childNodes[0].childNodes)
			{
				var title:String = songElt.attributes.title.toString();
				var artist:String = songElt.attributes.artist.toString();
				var album:String = songElt.attributes.album.toString();
				var path:URLRequest = new URLRequest(songElt.attributes.path.toString());
				
				this.addSong(new Song(path, title, artist, album));
			}
			
			fs.close();
			return true;
		}
		
		/**
		 * Remove a song from the playlist if it exists
		 * Return wether the song was removed
		 **/
		public function removeSong(s:Song):Boolean
		{
			for (var i:int =0; i<songs.length; ++i)
			{
				if (songs[i].getTitle() == s.getTitle() &&
					songs[i].getArtist() == s.getArtist() &&
					songs[i].getAlbum() == s.getAlbum())
				{
					songs.removeItemAt(i)
						return true;
				}
			}
					return false;
		}
		
	}
}